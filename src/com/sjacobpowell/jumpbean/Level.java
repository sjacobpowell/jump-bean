package com.sjacobpowell.jumpbean;

import static com.sjacobpowell.jumpbean.JumpBeanComponent.HEIGHT;
import static com.sjacobpowell.jumpbean.JumpBeanComponent.WIDTH;

import java.util.Random;

public class Level {
	public int xOffset;
	public Platform[] platforms;
	private Bitmap bitmap;

	public Level() {
		bitmap = new Bitmap(500, HEIGHT);
		platforms = new Platform[10];
		Random random = new Random();
		for (int i = 0; i < platforms.length; i++) {
			platforms[i] = new Platform(random.nextInt(2),
					5 + i * Platform.BITMAPS[0].width,
					HEIGHT - Platform.BITMAPS[0].height * (random.nextInt(6) + 1));
		}
		xOffset = 0;
	}

	private void drawPlatforms() {
		for (int i = 0; i < platforms.length; i++) {
			platforms[i].x -= xOffset;
			if(i > 0) {
				int speed = 1;
				platforms[i].y += i % 2 == 0 ? speed : -speed;
				platforms[i].y = platforms[i].y < 0 ? HEIGHT : platforms[i].y;
				platforms[i].y = platforms[i].y > HEIGHT ? 0 : platforms[i].y;
				platforms[i].x += i % 2 == 0 ? speed : -speed;
				platforms[i].x = platforms[i].x < 0 ? WIDTH : platforms[i].x;
				platforms[i].x = platforms[i].x > WIDTH ? 0 : platforms[i].x;
			}
			bitmap.draw(platforms[i].sprite, platforms[i].x, platforms[i].y);
		}
		xOffset = 0;
	}

	public Bitmap getBitmap() {
		bitmap.clear();
		drawPlatforms();
		return bitmap;
	}
}

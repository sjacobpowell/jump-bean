package com.sjacobpowell.jumpbean;

public class Platform extends Entity {
	public static Bitmap[] BITMAPS = getPlatformBitmaps();
	private static final int PLATFORM_COUNT = 2;

	public Platform(int...args) {
		switch(args.length) {
		case 0:
			initialize(0, 0, 0);
			break;
		case 1:
			initialize(args[0], 0, 0);
			break;
		case 2:
			initialize(args[0], args[1], 0);
			break;
		case 3:
		default:
			initialize(args[0], args[1], args[2]);
			break;
		}
	}

	private void initialize(int platformType, int x, int y) {
		sprite = BITMAPS[platformType >= 0 && platformType < PLATFORM_COUNT ? platformType : 0];
		this.x = x;
		this.y = y;
	}

	private static Bitmap[] getPlatformBitmaps() {
		Bitmap[] bitmaps = new Bitmap[PLATFORM_COUNT];
		for (int i = 0; i < PLATFORM_COUNT; i++) {
			bitmaps[i] = ArtTools.getBitmap("/res/background/platform" + i + ".png");
		}
		return bitmaps;
	}

}

package com.sjacobpowell.jumpbean;

import static com.sjacobpowell.jumpbean.ArtTools.getBitmap;
import static com.sjacobpowell.jumpbean.JumpBeanComponent.HEIGHT;
import static com.sjacobpowell.jumpbean.JumpBeanComponent.WIDTH;
import static com.sjacobpowell.jumpbean.JumpBeanComponent.SCALE;

public class Player extends Entity {
	private Level level;
	private Bitmap rightStand = getBitmap("/res/bean/rightStand.png");
	private Bitmap leftStand = getBitmap("/res/bean/leftStand.png");
	private Bitmap rightRise = getBitmap("/res/bean/rightRise.png");
	private Bitmap leftRise = getBitmap("/res/bean/leftRise.png");
	private Bitmap rightFall = getBitmap("/res/bean/rightFall.png");
	private Bitmap leftFall = getBitmap("/res/bean/leftFall.png");
	private Bitmap rightSlide = getBitmap("/res/bean/rightSlide.png");
	private Bitmap leftSlide = getBitmap("/res/bean/leftSlide.png");
	private Bitmap sprite = rightStand;
	private static final double SCREEN_BOUNDARY = 0;
	private final double RIGHT_SCREEN_BOUNDARY = WIDTH - SCREEN_BOUNDARY - sprite.width;
	private static final double LEFT_SCREEN_BOUNDARY = SCREEN_BOUNDARY;
	private static final double JUMP_SPEED = 8;
	private static final double WALK_ACCELERATION = .2;
	private static final double WALK_TOP_SPEED = 4;
	private static final double GRAVITY = .427;
	private static final int PLATFORM_OFFSET = 5;
	public long jumpCount = 0;
	public double y = 0;
	public double x = 0;
	public double jumpSpeed = JUMP_SPEED;
	public double speed = 0;
	private double platformXOffset = 0;
	private boolean rising = false;
	private boolean falling = true;
	private boolean released = true;
	private boolean movedRight = true;
	private boolean sliding = false;
	private Platform platform = null;

	public Player(Level level) {
		this.level = level;
	}

	public Bitmap getSprite() {
		if (rising) {
			sprite = movedRight ? rightRise : leftRise;
		}

		if (falling) {
			sprite = movedRight ? rightFall : leftFall;
		}

		if (!(rising || falling)) {
			sprite = speed == 0 || !sliding ? (movedRight ? rightStand : leftStand)
					: (speed > 0 ? rightSlide : leftSlide);
		}
		return sprite;
	}

	public void tick(boolean left, boolean right, boolean jump) {
		jump(jump);
		move(left, right);
	}

	private void move(boolean left, boolean right) {
		if (left || right) {
			if (left) {
				movedRight = false;
				speed -= WALK_ACCELERATION;
				speed = speed < -WALK_TOP_SPEED ? -WALK_TOP_SPEED : speed;
			}

			if (right) {
				movedRight = true;
				speed += WALK_ACCELERATION;
				speed = speed > WALK_TOP_SPEED ? WALK_TOP_SPEED : speed;
			}
			sliding = false;
		} else {
			if (speed > 0) {
				sprite = rightSlide;
				speed -= WALK_ACCELERATION;
			}
			if (speed < 0) {
				sprite = leftSlide;
				speed += WALK_ACCELERATION;
			}
			speed = Math.floor(speed) == 0 ? 0 : speed;
			sliding = true;
		}
		Platform newPlatform = getPlatform();
		platform = newPlatform == null ? platform : newPlatform;
		if(onPlatform()) {
			int platformX = platform.x;
			if(platformXOffset == 0) {
				platformXOffset = x - platformX;
			}
			x = getPlatform().x + platformXOffset;
		} else {
			if(platformXOffset != 0) {
				platformXOffset = 0;
				if(platform != null) {
					x = platform.x + platformXOffset;
				}
			} else {
				x += speed;
			}
		}

		if (x < LEFT_SCREEN_BOUNDARY) {
			x = LEFT_SCREEN_BOUNDARY;
			level.xOffset += speed;
		}
		if (x > RIGHT_SCREEN_BOUNDARY) {
			x = RIGHT_SCREEN_BOUNDARY;
			level.xOffset += speed;
		}
	}

	private void jump(boolean jump) {
		if (jump && !falling && released) {
			if (onPlatform() && rising == false) {
				rising = true;
				jumpCount++;
			}
			y -= jumpSpeed;
			jumpSpeed -= GRAVITY;
			if (jumpSpeed < 0) {
				falling = true;
				rising = false;
			}
		} else {
			if (onPlatform()) {
				rising = false;
				falling = false;
				jumpSpeed = JUMP_SPEED;
				released = !jump;
			} else {
				released = true;
				falling = true;
			}
			if (onPlatform()) {
				y = getPlatform().y - sprite.height + PLATFORM_OFFSET;
			} else {
				y += jumpSpeed;
				jumpSpeed += GRAVITY;
			}
		}
		if (dead()) {
//			System.exit(0);
			y = 0;
		}
	}

	private boolean dead() {
		return y + 50 >= HEIGHT * SCALE;
	}

	private boolean onPlatform() {
		return getPlatform() != null;
	}

	private Platform getPlatform() {
		int beanBottom = (int) (y + sprite.height);
		Platform platform;
		for (int i = 0; i < level.platforms.length; i++) {
			platform = level.platforms[i];
			if (Math.abs(platform.y - beanBottom) <= 15 && x + sprite.width >= platform.x + PLATFORM_OFFSET
					&& x <= platform.x + platform.sprite.width - PLATFORM_OFFSET) {
				return platform;
			}
		}
		return null;
	}

}

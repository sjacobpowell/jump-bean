package com.sjacobpowell.jumpbean;

import java.awt.event.KeyEvent;

public class Game {
	public int time;
	public Player player;
	public Level level;

	public Game() {
		level = new Level();
		player = new Player(level);
	}

	public void tick(boolean[] keys) {
		boolean left = keys[KeyEvent.VK_LEFT];
		boolean right = keys[KeyEvent.VK_RIGHT];
		boolean jump = keys[KeyEvent.VK_SPACE] || keys[KeyEvent.VK_UP];
		player.tick(left, right, jump);
		time++;
	}
}

package com.sjacobpowell.jumpbean;

import static com.sjacobpowell.jumpbean.ArtTools.getScaledBitmap;

public class Screen extends Bitmap {
	Bitmap background;

	public Screen(int width, int height) {
		super(width, height);
		background = getScaledBitmap("/res/background/countertop.jpg", width, height);
	}

	public void render(Game game) {
		clear();
		if (background != null) {
			draw(background, 0, 0);
		}
		draw(game.level.getBitmap(), 0, 0);
		draw(game.player.getSprite(), (int) game.player.x, (int) game.player.y);
	}

}

package com.sjacobpowell.jumpbean;

import java.applet.Applet;
import java.awt.BorderLayout;

public class JumpBeanApplet extends Applet {
	private static final long serialVersionUID = 1L;

	private JumpBeanComponent gameComponent = new JumpBeanComponent();

	public void init() {
		setLayout(new BorderLayout());
		add(gameComponent, BorderLayout.CENTER);
	}

	public void start() {
		gameComponent.start();
	}

	public void stop() {
		gameComponent.stop();
	}

}